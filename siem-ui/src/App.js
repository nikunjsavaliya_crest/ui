import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./components/HomePage/HomePage";
import "./App.css";
import MappingScreen from "./components/mappingScreen/mappingScreen";
import NavBar from "./components/NavBar/NavBar";

function App() {
  return (
    <>
      <NavBar />
      <Router>
        <Switch>
          <div className="App">
            <Route exact path="/" component={Home} />
            {/* <Route exact path="/" component={MappingScreen} /> */}
            <Route exact path="/mapping/:job" component={MappingScreen} />
            {/* <Home /> */}
          </div>
        </Switch>
      </Router>
    </>
  );
}

export default App;
