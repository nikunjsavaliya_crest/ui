import axios from "axios";

const BASE_URL = "http://10.0.8.26:5555";

const getJobs = () => {
  return axios.get(`${BASE_URL}/jobs/?skip=0&limit=20`);
};

const deleteJob = (job) => {
  return axios.delete(`${BASE_URL}/jobs/${job}`);
};

const createJob = (job, targetSiem, formData, config) => {
  return axios.post(
    `${BASE_URL}/jobs/?name=${job}&target_siem=${targetSiem}`,
    formData,
    config
  );
};

const getJobMapping = (job) => {
  return axios.get(`${BASE_URL}/mappings/${job}`);
};

const saveJobMapping = (job, data, config) => {
  return axios.put(`${BASE_URL}/mappings/${job}/`, data, config);
};

const getJobTransformation = (job) => {
  return axios.get(`${BASE_URL}/transforms/${job}`);
};

const saveLogstash = (job, data, config) => {
  return axios.put(`${BASE_URL}/transforms/${job}`, data, config);
};

export {
  getJobs,
  deleteJob,
  createJob,
  getJobMapping,
  saveJobMapping,
  getJobTransformation,
  saveLogstash,
};
