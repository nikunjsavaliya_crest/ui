import React, { useEffect, useState, Component } from "react";
import Preview from "../transformation/preview";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  makeStyles,
  Typography,
  Checkbox,
  IconButton,
  MenuItem,
  Menu,
  Button,
  Modal,
  CircularProgress,
  Snackbar,
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import SyntaxHighlighter from "react-syntax-highlighter";
import { stackoverflowLight } from "react-syntax-highlighter/dist/esm/styles/hljs";
import { Link } from "react-router-dom";
import { createFilter } from "react-select";
import CreatableSelect from "react-select/creatable";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-javascript";
import "ace-builds/src-noconflict/theme-github";
import { splunkDataModelOptions, elasticFieldOptions } from "./options";
import { FixedSizeList as List } from "react-window";
import { getJobMapping, saveJobMapping } from "../../api";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    margin: "10px 0 10px 0",
  },
  wrapper: { margin: "20px 80px", height: "100%" },
  accordionSummary: {
    display: "flex",
    flexDirection: "row-reverse",
    justifyContent: "center",
    height: "20px",
  },
  heading: {
    marginLeft: "20px",
    fontSize: "12px",
    fontWeight: "700",
  },
  accordionLabel: {
    fontSize: "16px",
    fontWeight: "900",
  },
  accordionSummaryContent: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    marginLeft: "10px",
  },
  accordion: {},
  innerAccordion: {
    margin: "0",
    border: "1px solid rgb(216,216,216,.87)",
  },
  accordionDetails: {
    display: "flex",
    flexDirection: "column",
  },
  innerAccordionSummary: {},
  formControl: {
    margin: theme.spacing(1),
    minWidth: 150,
  },
  acordionContent: {
    width: "100%",
    height: "auto",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    margin: "0 auto",
  },
  accordionContentWrapper: {
    display: "flex",
    justifyContent: "space-evenly",
    width: "100%",
  },
  sourceField: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    marginLeft: "50px",
    marginBottom: "10px",
    width: "auto",
  },
  targetField: {
    display: "flex",
    alignItems: "center",
    marginLeft: "50px",
    marginBottom: "10px",
  },
  targetContent: {
    width: "50%",
    borderLeft: "2px solid #d4d4d4",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  sourceContent: {
    width: "50%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
  },
  sourceFieldTypography: {
    // marginLeft: "50px",
    marginRight: "10px",
    fontSize: "12px",
  },
  sourceFieldTextfield: {
    fontSize: "12px",
    marginRight: "50px",
    marginLeft: "50px",
    height: "40px",
    width: "170px",
  },
  highlight: {
    margin: "20px 20px 0 20px",
    height: "10vh",
    width: "80%",
    padding: 0,
  },
  resize: {
    fontSize: "12px",
  },
  backBtn: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    background: "#e0e0e0",
    height: "40px",
    width: "50px",
  },
  headerContent: {
    margin: "40px 0",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  headerContentTypography: {
    marginLeft: "10px",
    fontSize: "22",
    fontWeight: "700",
  },

  modal: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  link: {
    TextDecoder: "none",
  },
}));

const Alert = (props) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
};

const accordionOptions = ["View", "Delete"];

const MappingScreen = (props) => {
  const classes = useStyles();
  const job = props.match.params.job;

  const [data, setData] = useState();
  const [anchorEl, setAnchorEl] = useState(null);
  const [openModal, setOpenModal] = useState(false);
  const [openSnack, setOpenSnack] = useState(false);
  const [snackbar, setSnackbar] = useState({
    severity: "success",
    message: "",
  });

  const handleSnackbarClick = () => {
    setOpenSnack(true);
  };

  const handleSnackbarClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpenSnack(false);
  };

  const open = Boolean(anchorEl);

  const handleOpen = () => {
    setOpenModal(true);
  };

  const handleModalClose = () => {
    setOpenModal(false);
  };

  const handleClick = (event) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (event) => {
    event.stopPropagation();
    setAnchorEl(null);
  };

  const handleSave = async () => {
    const config = {
      headers: {
        "content-type": "application/json",
      },
    };

    await saveJobMapping(job, data, config)
      .then((res) => {
        if (res.status === 200) {
          setSnackbar({
            severity: "success",
            message: "Job successfully saved",
          });
          handleSnackbarClick();
        } else if (res.status === 422) {
          setSnackbar({ severity: "error", message: "Something went wrong" });
          handleSnackbarClick();
        }
      })
      .catch((err) => {
        console.log(err.status);
      });
  };

  const body = <Preview job={job} handleModalClose={handleModalClose} />;

  const options = elasticFieldOptions;

  const height = 35;

  class MenuList extends Component {
    render() {
      const { options, children, maxHeight, getValue } = this.props;
      const [value] = getValue();
      const initialOffset = options.indexOf(value) * height;

      return (
        <List
          height={maxHeight}
          itemCount={children.length}
          itemSize={height}
          initialScrollOffset={initialOffset}
        >
          {({ index, style }) => <div style={style}>{children[index]}</div>}
        </List>
      );
    }
  }

  useEffect(() => {
    const fetchData = async () => {
      await getJobMapping(job).then((res) => {
        setData(res.data);
      });
    };
    fetchData();
  }, []);
  return (
    <div className={classes.wrapper}>
      <div className={classes.headerContent}>
        <div style={{ display: "flex", alignItems: "center" }}>
          <Link to="/" className={classes.link}>
            <Button variant="contained" className={classes.backBtn}>
              <ArrowBackIosIcon fontSize="small" />
            </Button>
          </Link>
          <div className={classes.headerContentTypography}>
            <Typography noWrap>{job}</Typography>
          </div>
        </div>
        <div
          style={{
            // marginLeft: "930px",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <Button
            variant="contained"
            color="primary"
            style={{
              color: "#fff",
              textTransform: "capitalize",
            }}
            onClick={handleSave}
          >
            Save
          </Button>

          <Button
            variant="contained"
            color="primary"
            style={{
              color: "#fff",
              textTransform: "capitalize",
              marginLeft: "20px",
            }}
            onClick={handleOpen}
          >
            Generate
          </Button>

          <Modal
            className={classes.modal}
            open={openModal}
            onClose={handleModalClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
          >
            {body}
          </Modal>
        </div>
      </div>
      {data ? (
        data.map((s, di) => {
          return (
            <div className={classes.root}>
              <Accordion
                className={classes.accordion}
                key={s.source_type}
                style={{ borderRadius: "10px" }}
              >
                <AccordionSummary
                  className={classes.accordionSummary}
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <div className={classes.accordionSummaryContent}>
                    <Typography
                      className={(classes.heading, classes.accordionLabel)}
                    >
                      {s.source}
                    </Typography>

                    <div>
                      <Checkbox
                        className={classes.checkbox}
                        value="check"
                        color="primary"
                        onClick={(e) => {
                          e.stopPropagation();
                        }}
                      />
                      <IconButton
                        aria-label="more"
                        aria-controls="long-menu"
                        aria-haspopup="true"
                        onClick={handleClick}
                      >
                        <MoreVertIcon />
                      </IconButton>
                      <Menu
                        id="long-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={open}
                        onClose={handleClose}
                        PaperProps={{
                          style: {
                            maxHeight: 48 * 4.5,
                            width: "20ch",
                          },
                        }}
                      >
                        {accordionOptions.map((option) => (
                          <MenuItem
                            key={option}
                            selected={option === "Pyxis"}
                            onClick={handleClose}
                          >
                            {option}
                          </MenuItem>
                        ))}
                      </Menu>
                    </div>
                  </div>
                </AccordionSummary>
                <AccordionDetails className={classes.accordionDetails}>
                  {s.mappings.map((m, mi) => {
                    const block_splunk = m.block.splunk;
                    const block_elastic = m.block.elastic;
                    const sourceField = m.source.map((src) => {
                      return src;
                    });
                    const targetField = m.target.map((target) => {
                      return target;
                    });
                    const field = m.source.map((src) => {
                      return src.field;
                    });
                    return (
                      <Accordion>
                        <AccordionSummary
                          expandIcon={<ExpandMoreIcon />}
                          aria-controls="panel1a-content"
                          id="panel1a-header"
                        >
                          <Typography className={classes.heading}>
                            {field.join(", ")}
                          </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                          <div className={classes.acordionContent}>
                            <div className={classes.accordionContentWrapper}>
                              <div className={classes.sourceContent}>
                                {sourceField.map((sf, si) => {
                                  return (
                                    <div className={classes.sourceField}>
                                      <div
                                        style={{
                                          display: "flex",
                                          justifyContent: "space-between",
                                          alignItems: "center",
                                          marginRight: "50px",
                                        }}
                                      >
                                        <CreatableSelect
                                          width="100px"
                                          value={{
                                            value: sf.data_model,
                                            label: sf.data_model,
                                          }}
                                          theme={(theme) => ({
                                            ...theme,
                                            // borderRadius: 0,
                                            colors: {
                                              ...theme.colors,
                                              primary25: "#757de8",
                                              primary: "#3f51b5  ",
                                            },
                                          })}
                                          placeholder="Data Modal"
                                          className={
                                            classes.sourceFieldTextfield
                                          }
                                          onChange={(selectedValue) => {
                                            const newData = [...data];
                                            newData[di].mappings[mi].source[
                                              si
                                            ].data_model = selectedValue.value;
                                            setData(newData);
                                            console.log(
                                              data[di].mappings[mi].source[si]
                                                .data_model
                                            );
                                          }}
                                          options={splunkDataModelOptions}
                                        />
                                        <span>
                                          <ArrowForwardIosIcon
                                            size="small"
                                            fontSize="small"
                                          />
                                        </span>
                                      </div>

                                      <Typography
                                        className={
                                          classes.sourceFieldTypography
                                        }
                                      >
                                        {sf.field}
                                      </Typography>
                                    </div>
                                  );
                                })}

                                {/* <SyntaxHighlighter
                                  language="json"
                                  style={stackoverflowLight}
                                  className={classes.highlight}
                                // wrapLongLines={true}
                                >
                                  {`${block_splunk}`}
                                </SyntaxHighlighter> */}
                                <AceEditor
                                  width="80%"
                                  height="20vh"
                                  value={`${block_splunk}`}
                                  showPrintMargin={false}
                                  mode="javascript"
                                  setOptions={{
                                    showLineNumbers: false,
                                  }}
                                  theme="github"
                                  name="UNIQUE_ID_OF_DIV"
                                  editorProps={{ $blockScrolling: true }}
                                  readOnly
                                />
                              </div>
                              <div className={classes.targetContent}>
                                {targetField.map((t, ti) => {
                                  return (
                                    <div className={classes.targetField}>
                                      <CreatableSelect
                                        filterOption={createFilter({
                                          ignoreAccents: false,
                                        })}
                                        components={{ MenuList }}
                                        width="100px"
                                        value={{
                                          value: t.field,
                                          label: t.field,
                                        }}
                                        theme={(theme) => ({
                                          ...theme,
                                          // borderRadius: 0,
                                          colors: {
                                            ...theme.colors,
                                            primary25: "#757de8",
                                            primary: "#3f51b5  ",
                                          },
                                        })}
                                        placeholder="Data Modal"
                                        className={classes.sourceFieldTextfield}
                                        onChange={(selectedValue) => {
                                          const newData = [...data];
                                          newData[di].mappings[mi].target[
                                            ti
                                          ].field = selectedValue.value;
                                          setData(newData);
                                          console.log(
                                            data[di].mappings[mi].target[ti]
                                              .field
                                          );
                                        }}
                                        options={options}
                                      />
                                    </div>
                                  );
                                })}
                                <AceEditor
                                  width="80%"
                                  height="20vh"
                                  value={`${block_elastic}`}
                                  showPrintMargin={false}
                                  mode="javascript"
                                  setOptions={{
                                    showLineNumbers: false,
                                  }}
                                  theme="github"
                                  name="UNIQUE_ID_OF_DIV"
                                  editorProps={{ $blockScrolling: true }}
                                  readOnly
                                />
                                {/* <SyntaxHighlighter
                                  language="javascript"
                                  style={stackoverflowLight}
                                  className={classes.highlight}
                                  // wrapLongLines={true}
                                >
                                  {`${block_elastic}`}
                                </SyntaxHighlighter> */}
                              </div>
                            </div>
                          </div>
                        </AccordionDetails>
                      </Accordion>
                    );
                  })}
                </AccordionDetails>
              </Accordion>
            </div>
          );
        })
      ) : (
        <CircularProgress />
      )}
      <Snackbar
        open={openSnack}
        autoHideDuration={3000}
        onClose={handleSnackbarClose}
      >
        <Alert onClose={handleSnackbarClose} severity={snackbar.severity}>
          {snackbar.message}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default MappingScreen;
