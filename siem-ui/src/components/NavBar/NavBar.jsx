import React from "react";
import { makeStyles, AppBar, Toolbar, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    margin: 0,
  },
  title: {
    fontSize: "30px medium",
  },
  version: {
    fontSize: "15px",
    fontWeight: 200,
  },
  toolBar: {
    display: "flex",
    justifyContent: "space-between",
  },
}));

export default function NavBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar className={classes.toolBar}>
          <Typography variant="h6" className={classes.title}>
            Universal Add-on Conversion Tool
          </Typography>
          <Typography variant="h6" className={classes.version}>
            v1.0.0
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}
