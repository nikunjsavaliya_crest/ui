import PropTypes from "prop-types";

import {
  CircularProgress,
  AppBar,
  Tabs,
  Tab,
  Typography,
  Box,
  Button,
  Snackbar,
  makeStyles,
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import React, { useState, useEffect } from "react";
import axios from "axios";
import AceEditor from "react-ace";
import { getJobTransformation, saveLogstash } from "../../api";
// import "ace-builds/src-noconflict/mode-javascript";
// import "ace-builds/src-noconflict/theme-github";

const Alert = (props) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
};

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  highlight: {
    height: "50vh",
    width: "100%",
  },
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    height: "20px",
  },
  ace: {
    width: "100%",
  },
  header: {
    fontWeight: "700",
    marginBottom: "10px",
  },
  btn_wrapper: {
    display: "flex",
    width: "100%",
    justifyContent: "flex-end",

    margin: "0 0 10px 0",
  },
  paper: {
    // display: "flex",
    // justifyContent: "center",
    // alignItems: "center",
    position: "absolute",
    width: 800,
    height: 550,
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 4, 3),
    outline: "none",
  },
  btn: {
    color: "#376fd0",
    textTransform: "capitalize",
  },
}));
const Preview = ({ job, handleModalClose }) => {
  const [keys, setKeys] = useState();
  const [logstashValues, setLogstashValues] = useState();
  const [data, setData] = useState();
  const [value, setValue] = useState(0);
  const [openSnack, setOpenSnack] = useState(false);
  const [snackbar, setSnackbar] = useState({
    severity: "success",
    message: "",
  });

  const handleSnackbarClick = () => {
    setOpenSnack(true);
  };

  const handleSnackbarClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenSnack(false);
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
    console.log(newValue);
  };

  const saveFile = async () => {
    const config = {
      headers: {
        "content-type": "application/json",
      },
    };
    const objData = {
      name: job,
      target_outputs: {},
    };

    keys.map((k, i) => {
      objData.target_outputs[k] = logstashValues[i];
    });
    await saveLogstash(job, objData, config).then((res) => {
      if (res.status === 200) {
        setSnackbar({
          severity: "success",
          message: "Job successfully saved",
        });
        handleSnackbarClick();
      } else if (res.status === 422) {
        setSnackbar({ severity: "error", message: "Something went wrong" });
        handleSnackbarClick();
      }
    });
  };

  const downloadFile = () => {
    const element = document.createElement("a");
    const file = new Blob([logstashValues[value]], {
      type: "text/plain",
    });
    element.href = URL.createObjectURL(file);
    element.download = `${keys[value]}.conf`;
    document.body.appendChild(element); // Required for this to work in FireFox
    element.click();
  };

  useEffect(() => {
    async function fetchData() {
      await getJobTransformation(job).then((res) => {
        setData(res.data);
        setKeys(Object.keys(res.data));
        setLogstashValues(Object.values(res.data));
      });
    }
    fetchData();
  }, []);

  const classes = useStyles();
  return (
    <div className={classes.paper}>
      <div className={classes.root}>
        <Typography className={classes.header}>Preview</Typography>
        {logstashValues ? (
          <>
            <AppBar position="static" color="default">
              <Tabs
                tabItemContainerStyle={{ height: 30 }}
                value={value}
                onChange={handleChange}
                indicatorColor="primary"
                textColor="primary"
                variant="scrollable"
                scrollButtons="auto"
                aria-label="scrollable auto tabs example"
              >
                {keys.map((d, i) => {
                  return <Tab label={d} {...a11yProps(i)} />;
                })}
              </Tabs>
            </AppBar>
            {logstashValues.map((t, i) => {
              return (
                <TabPanel value={value} index={i}>
                  <AceEditor
                    width="100%"
                    height="400px"
                    value={`${t}`}
                    showPrintMargin={false}
                    mode="javascript"
                    theme="github"
                    onChange={(newValue) => {
                      logstashValues[i] = newValue;
                    }}
                    name="UNIQUE_ID_OF_DIV"
                    editorProps={{ $blockScrolling: true }}
                  />
                </TabPanel>
              );
            })}
            <div className={classes.btn_wrapper}>
              <Button className={classes.btn} onClick={handleModalClose}>
                Cancel
              </Button>
              <Button className={classes.btn} onClick={saveFile}>
                Save
              </Button>
              <Button className={classes.btn} onClick={downloadFile}>
                Download
              </Button>
            </div>
          </>
        ) : (
          <CircularProgress />
        )}
        <Snackbar
          open={openSnack}
          autoHideDuration={6000}
          onClose={handleSnackbarClose}
        >
          <Alert onClose={handleSnackbarClose} severity={snackbar.severity}>
            {snackbar.message}
          </Alert>
        </Snackbar>
      </div>
    </div>
  );
};

export default Preview;
