import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
  MenuItem,
  Snackbar,
  Typography,
  TextField,
  Paper,
  Button,
  Select,
  Divider,
  createStyles,
  makeStyles,
} from "@material-ui/core";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import MuiAlert from "@material-ui/lab/Alert";
import { createJob } from "../../api";

const Alert = (props) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
};

const useStyles = makeStyles((theme) =>
  createStyles({
    wrapper: {
      display: "flex",
      justifyContent: "center",
    },
    root: {
      padding: "10px",
      marginTop: "50px",
      marginLeft: "50px",
      borderRadius: "10px",
      width: "600px",
      height: "auto",
    },
    headerText: {
      padding: "12px",
      paddingLeft: "7%",
    },
    targetForm: {
      // padding: "2%",
      paddingLeft: "7%",
    },
    jobName: {
      display: "flex",
      alignItems: "center",
    },
    targetSiem: {
      display: "flex",
      alignItems: "center",
    },
    splFile: {
      display: "flex",
      alignItems: "center",
    },
    formControl: {
      margin: "1.5% 0% !important",
    },
    fileControl: {
      margin: "3% 0% !important",
      display: "flex",
      alignItems: "center",
    },
    formButton: {
      color: "rgb(55,111,208)",
      "&:hover": {
        background: "rgba(55,111,208, 0.05)",
      },
      textDecoration: "none",
      textTransform: "none",
    },
    fileButton: {
      textTransform: "none",
    },
    link: {
      textDecoration: "none",
    },
    buttonControl: {
      display: "flex",
      justifyContent: "flex-end",
      paddingRight: "3%",
    },
  })
);

const Extraction = (props) => {
  const classes = useStyles();

  const [file, setFile] = useState(null);
  const [jobName, setJobName] = useState("");
  const [target_siem, setTarget_siem] = useState("Elastic");
  const hiddenFileInput = React.useRef(null);
  const [openSnack, setOpenSnack] = useState(false);
  const [snackbar, setSnackbar] = useState({
    severity: "success",
    message: "",
  });

  const handleSnackbarClick = () => {
    setOpenSnack(true);
  };

  const handleSnackbarClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenSnack(false);
  };

  const handleClick = (event) => {
    hiddenFileInput.current.click();
  };

  const handleChange = (event) => {
    const fileUploaded = event.target.files[0];
    console.log(fileUploaded);
    setFile(fileUploaded);
  };

  const handleSubmit = async (e) => {
    const formData = new FormData();
    formData.append("splunk_addon", file);
    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };
    await createJob(jobName, target_siem, formData, config)
      .then((res) => {
        if (res.status === 201) {
          setSnackbar({
            severity: "success",
            message: "Job successfully created",
          });
          handleSnackbarClick();
          window.location.href = `/mapping/${jobName}`;
        } else if (res.status === 422) {
          setSnackbar({ severity: "error", message: "Something went wrong" });
          handleSnackbarClick();
        } else if (res.status === 409) {
          setSnackbar({
            severity: "error",
            message: "Job name already exists",
          });
          handleSnackbarClick();
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className={classes.wrapper}>
      <Paper className={classes.root}>
        <Typography className={classes.headerText} variant="h5">
          Create Job
        </Typography>
        <Divider />
        <div className={classes.targetForm}>
          {/* <Grid container spacing={3}>
            <Grid item xs={9} className={classes.gridSpace}> */}
          <div className={classes.jobName}>
            <Typography style={{ width: "130px", fontSize: "14px" }}>
              Job Name*
            </Typography>
            <div className={classes.formControl}>
              <TextField
                style={{ width: "250px", height: "40px" }}
                id="name"
                required
                variant="outlined"
                margin="normal"
                size="small"
                value={jobName}
                onChange={(e) => {
                  setJobName(e.target.value);
                }}
              />
            </div>
          </div>
          <div className={classes.targetSiem}>
            <Typography style={{ width: "130px", fontSize: "14px" }}>
              Target SIEM*
            </Typography>
            <div className={classes.formControl}>
              <Select
                style={{ width: "250px", height: "40px" }}
                id="target_siem"
                required
                variant="outlined"
                margin="normal"
                size="small"
                value={target_siem}
                onChange={(e, i, value) => {
                  console.log(value);
                  // setTarget_siem(value);
                }}
              >
                <MenuItem value="Elastic">Elastic</MenuItem>
              </Select>
            </div>
          </div>
          {/* </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={4}> */}
          <div className={classes.splFile}>
            <Typography style={{ width: "130px", fontSize: "14px" }}>
              SPL File*
            </Typography>
            <div className={classes.fileControl}>
              <Button
                color="black"
                size="medium"
                variant="contained"
                startIcon={<CloudUploadIcon />}
                className={classes.fileButton}
                onClick={handleClick}
              >
                Choose file...
              </Button>
              <Typography style={{ display: "inline", marginLeft: "10px" }}>
                {file ? file.name : ""}
              </Typography>
              <input
                type="file"
                ref={hiddenFileInput}
                onChange={handleChange}
                style={{ display: "none" }}
              />
            </div>
          </div>
          {/* </Grid>
          </Grid> */}
          {/* <div> */}
          {/* <Grid container spacing={3}>
              <Grid item xs={12}> */}
          <div className={classes.buttonControl}>
            <Link className={classes.link}>
              <Button
                color="white"
                size="medium"
                className={classes.formButton}
                // href="/mapping"
                onClick={handleSubmit}
              >
                Create
              </Button>
            </Link>
            <Button
              size="medium"
              className={classes.formButton}
              onClick={props.handleClose}
            >
              Cancel
            </Button>
          </div>
          {/* </Grid>
            </Grid>
          </div> */}
        </div>
      </Paper>
      <Snackbar
        open={openSnack}
        autoHideDuration={6000}
        onClose={handleSnackbarClose}
      >
        <Alert onClose={handleSnackbarClose} severity={snackbar.severity}>
          {snackbar.message}
        </Alert>
      </Snackbar>
    </div>
  );
};
export default Extraction;
