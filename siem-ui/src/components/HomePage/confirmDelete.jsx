import React from "react";
import {
  Button,
  createStyles,
  makeStyles,
  Paper,
  Typography,
} from "@material-ui/core";
import WarningIcon from "@material-ui/icons/Warning";
const useStyles = makeStyles((theme) =>
  createStyles({
    wrapper: {
      padding: "15px",
      width: "400px",
    },
    textContent: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
      marginTop: "20px",
    },
    warningIcon: {
      color: "red",
    },
    flexCenter: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    headerText: {
      //   padding: "12px",
      fontSize: "16px",
      fontWeight: "500",
    },
    buttonControl: {
      marginTop: "20px",
      display: "flex",
      justifyContent: "flex-end",
    },
    confirmText: {
      fontSize: "14px",
    },
    formButton: {
      color: "rgb(55,111,208)",
      "&:hover": {
        background: "rgba(55,111,208, 0.05)",
      },
      textDecoration: "none",
      textTransform: "none",
    },
  })
);
const ConfirmDelete = ({ handleConfirmDeleteClose, handleDelete }) => {
  const classes = useStyles();
  return (
    <Paper className={classes.wrapper}>
      <Typography className={classes.headerText}>Delete Job</Typography>
      <div className={classes.content}>
        <div className={classes.textContent}>
          <div className={classes.flexCenter}>
            <WarningIcon className={classes.warningIcon} fontSize="large" />
          </div>
          <Typography className={classes.confirmText}>
            Are you sure you want to delete this job?
          </Typography>
        </div>
        <div>
          <div className={classes.buttonControl}>
            <Button
              color="white"
              size="medium"
              className={classes.formButton}
              onClick={handleDelete}
            >
              Delete
            </Button>

            <Button
              size="medium"
              className={classes.formButton}
              onClick={handleConfirmDeleteClose}
            >
              Cancel
            </Button>
          </div>
        </div>
      </div>
    </Paper>
  );
};

export default ConfirmDelete;
