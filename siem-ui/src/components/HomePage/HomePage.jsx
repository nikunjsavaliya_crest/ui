import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import {
  Paper,
  Snackbar,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableHead,
  TableRow,
  Modal,
  Button,
  IconButton,
  createStyles,
  makeStyles,
  CircularProgress,
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import VisibilityIcon from "@material-ui/icons/Visibility";
import Extraction from "./Extraction";
import ConfirmDelete from "./confirmDelete";
import { deleteJob, getJobs } from "../../api";
import Preview from "../transformation/preview";

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      height: "100%",
      width: "auto",
    },
    wrapper: {
      display: "flex",
      justifyContent: "flex-end",
      marginLeft: "80px",
      marginRight: "80px",
    },
    btn: {
      marginTop: "30px",
      textTransform: "none",
    },
    link: {
      textDecoration: "none",
    },
    extractTableContainer: {
      marginTop: "3vh !important",
    },
    iconButton: {
      marginLeft: "25px",
      padding: "4px 0px",
    },
    landing: {
      padding: "10px",
      marginTop: "30px",
      marginLeft: "80px",
      marginRight: "80px",
      borderRadius: "10px",
    },
    disclaimer: {
      display: "flex",
      alignItems: "left",
      fontSize: "10px",
    },
    noteContent: {
      textAlign: "left",
      fontSize: "12px",
      marginLeft: "15px !important",
    },
    disclaimerContent: {
      textAlign: "left",
      marginLeft: "-25px !important",
      marginTop: "15px",
    },
    modelPopUp: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
  })
);

const Alert = (props) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
};

const HomePage = () => {
  const [jobs, setJobs] = useState();
  const [jobName, setJobName] = useState();
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [preview, setPreview] = useState(false);
  const [page, setPage] = useState(0);
  const [confirmDelete, setConfirmDelete] = useState(false);
  const [openSnack, setOpenSnack] = useState(false);
  const [snackbar, setSnackbar] = useState({
    severity: "success",
    message: "",
  });

  const handleSnackbarClick = () => {
    setOpenSnack(true);
  };

  const handleSnackbarClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenSnack(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleConfirmDeleteOpen = (job) => {
    setJobName(job);
    setConfirmDelete(true);
  };

  const handleConfirmDeleteClose = () => {
    setConfirmDelete(false);
  };

  const handlePreviewOpen = (job) => {
    setJobName(job);
    setPreview(true);
  };

  const handlePreviewClose = () => {
    setPreview(false);
  };
  const handleDelete = async () => {
    await deleteJob(jobName).then((res) => {
      if (res.status === 200) {
        setSnackbar({
          severity: "success",
          message: "Job successfully Deleted",
        });
        handleConfirmDeleteClose();
        handleSnackbarClick();
        const filtered = jobs.filter((el) => {
          return el.name !== jobName;
        });
        setJobs(filtered);
      } else if (res.status === 422) {
        handleSnackbarClick();
      }
    });
  };

  const handleCopy = () => {
    setSnackbar({ severity: "error", message: "Something went wrong" });
    handleSnackbarClick();
  };

  const extractionBody = <Extraction handleClose={handleClose} />;
  const confirmDeleteBody = (
    <ConfirmDelete
      handleConfirmDeleteClose={handleConfirmDeleteClose}
      handleDelete={handleDelete}
    />
  );

  const previewBody = (
    <Preview job={jobName} handleModalClose={handlePreviewClose} />
  );

  const rowsPerPage = 5;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  useEffect(() => {
    const fetchData = async () => {
      await getJobs().then((res) => {
        const data = res.data;
        const jobs_data = [
          ...new Map(data.map((item) => [item["name"], item])).values(),
        ];

        setJobs(jobs_data);
      });
    };
    fetchData();
  }, []);

  return (
    <div className={classes.root}>
      <div className={classes.wrapper}>
        <Button
          className={classes.btn}
          variant="contained"
          aria-label="primary"
          color="primary"
          onClick={handleOpen}
        >
          + Create
        </Button>
        <Modal
          className={classes.modelPopUp}
          open={open}
          onClose={handleClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          {extractionBody}
        </Modal>
        <Modal
          className={classes.modelPopUp}
          open={confirmDelete}
          onClose={handleConfirmDeleteClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          {confirmDeleteBody}
        </Modal>
        <Modal
          className={classes.modelPopUp}
          open={preview}
          onClose={handlePreviewClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
        >
          {previewBody}
        </Modal>
      </div>
      {jobs ? (
        <>
          {" "}
          <Paper className={classes.landing}>
            <TableContainer className={classes.extractTableContainer}>
              <Table
                size="small"
                aria-label="sticky table"
                className="extraction-table"
              >
                <TableHead>
                  <TableRow>
                    <TableCell size="medium" align="left">
                      Job Name
                    </TableCell>
                    <TableCell size="medium" align="left">
                      Target SIEM
                    </TableCell>
                    <TableCell
                      size="medium"
                      align="right"
                      style={{ paddingRight: "4%" }}
                    >
                      Actions
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {jobs
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((job) => {
                      return (
                        <TableRow>
                          <TableCell align="left">{job.name}</TableCell>
                          <TableCell align="left">{job.target_siem}</TableCell>
                          <TableCell align="right">
                            <div className={classes.actionIcons}>
                              <IconButton
                                size="small"
                                title="View"
                                color="primary"
                                className={classes.iconButton}
                                onClick={() => {
                                  handlePreviewOpen(job.name);
                                }}
                              >
                                <VisibilityIcon />
                              </IconButton>
                              <Link to={`/mapping/${job.name}`}>
                                <IconButton
                                  size="small"
                                  title="Edit"
                                  color="primary"
                                  className={classes.iconButton}
                                >
                                  <EditIcon />
                                </IconButton>
                              </Link>
                              <IconButton
                                size="small"
                                title="Delete"
                                color="primary"
                                className={classes.iconButton}
                                onClick={() => {
                                  handleConfirmDeleteOpen(job.name);
                                }}
                              >
                                <DeleteIcon />
                              </IconButton>
                            </div>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              labelRowsPerPage=""
              rowsPerPageOptions={[]}
              component="div"
              count={jobs.length}
              rowsPerPage={5}
              page={page}
              onChangePage={handleChangePage}
            />
            <div className={classes.disclaimer}>
              <div className={classes.disclaimerTitle}>Disclaimer:</div>
              <ui className={classes.disclaimerContent}>
                <li>
                  To directly view and edit the generated output, click on the
                  eye icon.
                </li>
                <li>
                  To view and edit the mappings and regenerate the output, click
                  on the edit icon.
                </li>
              </ui>
            </div>
          </Paper>
        </>
      ) : (
        <CircularProgress />
      )}
      <Snackbar
        open={openSnack}
        autoHideDuration={6000}
        onClose={handleSnackbarClose}
      >
        <Alert onClose={handleSnackbarClose} severity={snackbar.severity}>
          {snackbar.message}
        </Alert>
      </Snackbar>
    </div>
  );
};

export default HomePage;
